BREAKING: Delete IntegerMinutes::as_days, which returned a number of minutes !
ADDED: Provide IntegerMinutes::as_minutes
